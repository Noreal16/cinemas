import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'models/products_repository.dart';
import 'models/product.dart';


class HomePage extends StatelessWidget{
  List<Card> _builGridCards(BuildContext context){
    List<Product> products = ProductsRepository.loadProducts(Category.all);

    if(products == null || products.isEmpty){
      return const <Card>[];
    }
    final ThemeData theme = Theme.of(context);
    final NumberFormat formatter = NumberFormat.simpleCurrency(
      locale: Localizations.localeOf(context).toString()
    );
    return products.map((products){
      return Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 18/11,
              child: Image.asset(
                products.assetName,
                package: products.assetPackage,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      products.name,
                      style: theme.textTheme.title,
                      maxLines: 1,
                    ),
                    SizedBox(height: 8.0,),
                    Text(
                      formatter.format(products.price),
                      style: theme.textTheme.body2,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }).toList();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //es la barra superior de la aplicacion
      appBar: AppBar(
        //configure un IconButton para el leading:campo de la Barra de aplicaciones . (Póngalo antes del title:campo para imitar el orden de inicio a final):
        //crea botones en la parte izquierda de la app
        leading: IconButton(
          icon:Icon(
              Icons.menu,
              semanticLabel: 'menu',
          ) ,
          onPressed: (){
            print('Menu button');
          },
        ),
        title: Text('Bienvenido'),
        //crea botones en la parte derecha de la aplicacion
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              semanticLabel: 'Search',
            ),
            onPressed: (){
              print('Search Button');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.tune,
              semanticLabel: 'filter',
            ),
            onPressed: (){
              print('Filter Button');
            },
          ),
        ],
      ),
      //Vamos a desempaquetar ese código. GridView invoca al count()constructor ya que la cantidad de elementos que muestra es contable y no infinita. Pero necesita información para definir su diseño.
      body: GridView.count(
        //El crossAxisCount:especifica cuántos elementos hay. Queremos 2 columnas.
        crossAxisCount: 2,
        //El padding:campo proporciona espacio en los 4 lados de GridView.
        padding: EdgeInsets.all(16.0),
        //El childAspectRatio:campo identifica el tamaño de los elementos en función de una relación de aspecto (ancho sobre alto).
        childAspectRatio: 8.0 / 9.0,
        children: _builGridCards(context),
      ),
      // para evitar el teclado en pantalla cuya altura está definida por el ambiente
      resizeToAvoidBottomInset: false,
    );
  }

}